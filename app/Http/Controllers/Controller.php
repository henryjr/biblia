<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    
    public function index()
    {
        return view('home');
    }
    public function projeto()
    {
        return view('projeto/projeto');
    }

    public function access()
    {
        return view('user/access');
    }
    
}