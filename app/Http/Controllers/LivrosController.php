<?php

namespace App\Http\Controllers;

use App\Models\Livros;
use DB;

class LivrosController extends Controller
{
    public function index()
    {
        $antigo = Livros::all();
        $novo = Livros::all();
        return view('biblia/biblia', ['antigo' => $antigo, 'novo' => $novo]);
    }

    public function getLivro($liv_abr)
    {
        $Livro = DB::table('livros')
            ->where('liv_abr', $liv_abr)
            ->get();

        $Capitulos = DB::table('livros')
            ->where('liv_abr', $liv_abr)
            ->join('versiculos', 'ver_liv_id', '=', 'livros.id')
            ->where('ver_vrs_id', '=', '0')
            ->orderBy('ver_capitulo', 'ASC')
            ->get();

        $Count = DB::table('livros')
            ->where('liv_abr', $liv_abr)
            ->join('versiculos', 'ver_liv_id', '=', 'livros.id')
            ->select(DB::raw('distinct ver_capitulo'))
            ->groupBy('ver_capitulo')
            ->orderBy('ver_capitulo', 'DESC')
            ->first();

        return view(
            'biblia/livro',
            [
                'Livro' => $Livro,
                'Capitulos' => $Capitulos,
                'Count' => $Count
            ]
        );
    }

    public function getLeitura($liv_abr, $ver_capitulo)
    {
        $Livro = DB::table('livros')
            ->where('liv_abr', $liv_abr)
            ->get();

        $Versiculos = DB::table('livros')
            ->where('liv_abr', $liv_abr)
            ->join('versiculos', 'ver_liv_id', '=', 'livros.id')
            ->where('ver_capitulo', $ver_capitulo)
            ->where('ver_vrs_id', '=', '0')
            ->get();

        $Count = DB::table('livros')
            ->where('liv_abr', $liv_abr)
            ->join('versiculos', 'ver_liv_id', '=', 'livros.id')
            ->select(DB::raw('distinct ver_capitulo'))
            ->groupBy('ver_capitulo')
            ->orderBy('ver_capitulo', 'DESC')
            ->first();

        return view('biblia/leitura',
            [
                'Livro' => $Livro,
                'Versiculos' => $Versiculos,
                'Count' => $Count,
                'Capitulo' => $ver_capitulo
            ]);
    }

}
