<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'Controller@index');
$app->get('/project', 'Controller@projeto');
$app->get('/access', 'Controller@access');
$app->get('/biblia', 'LivrosController@index');
$app->get('/biblia/{liv_abr}', 'LivrosController@getLivro');
$app->get('/biblia/{liv_abr}/{ver_capitulo}', 'LivrosController@getLeitura');
