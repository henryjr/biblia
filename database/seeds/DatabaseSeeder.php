<?php

use App\Models\Versoes;
use App\Models\Livros;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call('VersoesTableSeeder');
        $this->command->info('VersoesTableSeeder table seeded!');
        $this->call('LivrosTableSeeder');
        $this->command->info('LivrosTableSeeder table seeded!');

    }
}


class VersoesTableSeeder extends Seeder
{
    public function run()
    {
        Versoes::create(['vrs_id' => '0', 'vrs_nome' => 'Não Identificada']);
        Versoes::create(['vrs_id' => '1', 'vrs_nome' => 'Almeida Revisada Imprensa Bíblica']);
        Versoes::create(['vrs_id' => '2', 'vrs_nome' => 'Almeida Corrigida e Revisada Fiel']);
        Versoes::create(['vrs_id' => '3', 'vrs_nome' => 'Nova Versão Internacional']);
        Versoes::create(['vrs_id' => '4', 'vrs_nome' => 'Sociedade Bíblica Britânica']);
        Versoes::create(['vrs_id' => '5', 'vrs_nome' => 'Almeida Revista e Atualizada']);
    }
}

class LivrosTableSeeder extends Seeder
{
    public function run()
    {
        Livros::create(['liv_id' => '1', 'liv_tes_id' => '1', 'liv_posicao' => '1', 'liv_nome' => 'Gênesis']);
        Livros::create(['liv_id' => '2', 'liv_tes_id' => '1', 'liv_posicao' => '2', 'liv_nome' => 'Exôdo']);
        Livros::create(['liv_id' => '3', 'liv_tes_id' => '1', 'liv_posicao' => '3', 'liv_nome' => 'Levítico']);
        Livros::create(['liv_id' => '4', 'liv_tes_id' => '1', 'liv_posicao' => '4', 'liv_nome' => 'Números']);
        Livros::create(['liv_id' => '5', 'liv_tes_id' => '1', 'liv_posicao' => '5', 'liv_nome' => 'Deuteronômio']);
        Livros::create(['liv_id' => '6', 'liv_tes_id' => '1', 'liv_posicao' => '6', 'liv_nome' => 'Josué']);
        Livros::create(['liv_id' => '7', 'liv_tes_id' => '1', 'liv_posicao' => '7', 'liv_nome' => 'Juízes']);
        Livros::create(['liv_id' => '8', 'liv_tes_id' => '1', 'liv_posicao' => '8', 'liv_nome' => 'Rute']);
        Livros::create(['liv_id' => '9', 'liv_tes_id' => '1', 'liv_posicao' => '9', 'liv_nome' => 'I Samuel']);
        Livros::create(['liv_id' => '10', 'liv_tes_id' => '1', 'liv_posicao' => '10', 'liv_nome' => 'II Samuel']);
        Livros::create(['liv_id' => '11', 'liv_tes_id' => '1', 'liv_posicao' => '11', 'liv_nome' => 'I Reis']);
        Livros::create(['liv_id' => '12', 'liv_tes_id' => '1', 'liv_posicao' => '12', 'liv_nome' => 'II Reis']);
        Livros::create(['liv_id' => '13', 'liv_tes_id' => '1', 'liv_posicao' => '13', 'liv_nome' => 'I Crônicas']);
        Livros::create(['liv_id' => '14', 'liv_tes_id' => '1', 'liv_posicao' => '14', 'liv_nome' => 'II Crônicas']);
        Livros::create(['liv_id' => '15', 'liv_tes_id' => '1', 'liv_posicao' => '15', 'liv_nome' => 'Esdras']);
        Livros::create(['liv_id' => '16', 'liv_tes_id' => '1', 'liv_posicao' => '16', 'liv_nome' => 'Neemias']);
        Livros::create(['liv_id' => '17', 'liv_tes_id' => '1', 'liv_posicao' => '17', 'liv_nome' => 'Ester']);
        Livros::create(['liv_id' => '18', 'liv_tes_id' => '1', 'liv_posicao' => '18', 'liv_nome' => 'Jo']);
        Livros::create(['liv_id' => '19', 'liv_tes_id' => '1', 'liv_posicao' => '19', 'liv_nome' => 'Salmos']);
        Livros::create(['liv_id' => '20', 'liv_tes_id' => '1', 'liv_posicao' => '20', 'liv_nome' => 'Provérbios']);
        Livros::create(['liv_id' => '21', 'liv_tes_id' => '1', 'liv_posicao' => '21', 'liv_nome' => 'Eclesiastes']);
        Livros::create(['liv_id' => '22', 'liv_tes_id' => '1', 'liv_posicao' => '22', 'liv_nome' => 'Cantico dos Canticos']);
        Livros::create(['liv_id' => '23', 'liv_tes_id' => '1', 'liv_posicao' => '23', 'liv_nome' => 'Isaías']);
        Livros::create(['liv_id' => '24', 'liv_tes_id' => '1', 'liv_posicao' => '24', 'liv_nome' => 'Jeremias']);
        Livros::create(['liv_id' => '25', 'liv_tes_id' => '1', 'liv_posicao' => '25', 'liv_nome' => 'Lamentações Jeremias']);
        Livros::create(['liv_id' => '26', 'liv_tes_id' => '1', 'liv_posicao' => '26', 'liv_nome' => 'Ezequiel']);
        Livros::create(['liv_id' => '27', 'liv_tes_id' => '1', 'liv_posicao' => '27', 'liv_nome' => 'Daniel']);
        Livros::create(['liv_id' => '28', 'liv_tes_id' => '1', 'liv_posicao' => '28', 'liv_nome' => 'Oséias']);
        Livros::create(['liv_id' => '29', 'liv_tes_id' => '1', 'liv_posicao' => '29', 'liv_nome' => 'Joel']);
        Livros::create(['liv_id' => '30', 'liv_tes_id' => '1', 'liv_posicao' => '30', 'liv_nome' => 'Amos']);
        Livros::create(['liv_id' => '31', 'liv_tes_id' => '1', 'liv_posicao' => '31', 'liv_nome' => 'Obadias']);
        Livros::create(['liv_id' => '32', 'liv_tes_id' => '1', 'liv_posicao' => '32', 'liv_nome' => 'Jonas']);
        Livros::create(['liv_id' => '33', 'liv_tes_id' => '1', 'liv_posicao' => '33', 'liv_nome' => 'Miqueias']);
        Livros::create(['liv_id' => '34', 'liv_tes_id' => '1', 'liv_posicao' => '34', 'liv_nome' => 'Naum']);
        Livros::create(['liv_id' => '35', 'liv_tes_id' => '1', 'liv_posicao' => '35', 'liv_nome' => 'Habacuque']);
        Livros::create(['liv_id' => '36', 'liv_tes_id' => '1', 'liv_posicao' => '36', 'liv_nome' => 'Sofonias']);
        Livros::create(['liv_id' => '37', 'liv_tes_id' => '1', 'liv_posicao' => '37', 'liv_nome' => 'Ageu']);
        Livros::create(['liv_id' => '38', 'liv_tes_id' => '1', 'liv_posicao' => '38', 'liv_nome' => 'Zacarias']);
        Livros::create(['liv_id' => '39', 'liv_tes_id' => '1', 'liv_posicao' => '39', 'liv_nome' => 'Malaquias']);
        Livros::create(['liv_id' => '40', 'liv_tes_id' => '2', 'liv_posicao' => '1', 'liv_nome' => 'Mateus']);
        Livros::create(['liv_id' => '41', 'liv_tes_id' => '2', 'liv_posicao' => '2', 'liv_nome' => 'Marcos']);
        Livros::create(['liv_id' => '42', 'liv_tes_id' => '2', 'liv_posicao' => '3', 'liv_nome' => 'Lucas']);
        Livros::create(['liv_id' => '43', 'liv_tes_id' => '2', 'liv_posicao' => '4', 'liv_nome' => 'João']);
        Livros::create(['liv_id' => '44', 'liv_tes_id' => '2', 'liv_posicao' => '5', 'liv_nome' => 'Atos']);
        Livros::create(['liv_id' => '45', 'liv_tes_id' => '2', 'liv_posicao' => '6', 'liv_nome' => 'Romanos']);
        Livros::create(['liv_id' => '46', 'liv_tes_id' => '2', 'liv_posicao' => '7', 'liv_nome' => 'I Coríntios']);
        Livros::create(['liv_id' => '47', 'liv_tes_id' => '2', 'liv_posicao' => '8', 'liv_nome' => 'II Coríntios']);
        Livros::create(['liv_id' => '48', 'liv_tes_id' => '2', 'liv_posicao' => '9', 'liv_nome' => 'Galatas']);
        Livros::create(['liv_id' => '49', 'liv_tes_id' => '2', 'liv_posicao' => '10', 'liv_nome' => 'Efesios']);
        Livros::create(['liv_id' => '50', 'liv_tes_id' => '2', 'liv_posicao' => '11', 'liv_nome' => 'Filipenses']);
        Livros::create(['liv_id' => '51', 'liv_tes_id' => '2', 'liv_posicao' => '12', 'liv_nome' => 'Colossenses']);
        Livros::create(['liv_id' => '52', 'liv_tes_id' => '2', 'liv_posicao' => '13', 'liv_nome' => 'I Tessalonicenses']);
        Livros::create(['liv_id' => '53', 'liv_tes_id' => '2', 'liv_posicao' => '14', 'liv_nome' => 'II Tessalonicenses']);
        Livros::create(['liv_id' => '54', 'liv_tes_id' => '2', 'liv_posicao' => '15', 'liv_nome' => 'I Timoteo']);
        Livros::create(['liv_id' => '55', 'liv_tes_id' => '2', 'liv_posicao' => '16', 'liv_nome' => 'II Timoteo']);
        Livros::create(['liv_id' => '56', 'liv_tes_id' => '2', 'liv_posicao' => '17', 'liv_nome' => 'Tito']);
        Livros::create(['liv_id' => '57', 'liv_tes_id' => '2', 'liv_posicao' => '18', 'liv_nome' => 'Filemom']);
        Livros::create(['liv_id' => '58', 'liv_tes_id' => '2', 'liv_posicao' => '19', 'liv_nome' => 'Hebreus']);
        Livros::create(['liv_id' => '59', 'liv_tes_id' => '2', 'liv_posicao' => '20', 'liv_nome' => 'Tiago']);
        Livros::create(['liv_id' => '60', 'liv_tes_id' => '2', 'liv_posicao' => '21', 'liv_nome' => 'I Pedro']);
        Livros::create(['liv_id' => '61', 'liv_tes_id' => '2', 'liv_posicao' => '22', 'liv_nome' => 'II Pedro']);
        Livros::create(['liv_id' => '62', 'liv_tes_id' => '2', 'liv_posicao' => '23', 'liv_nome' => 'I João']);
        Livros::create(['liv_id' => '63', 'liv_tes_id' => '2', 'liv_posicao' => '24', 'liv_nome' => 'II João']);
        Livros::create(['liv_id' => '64', 'liv_tes_id' => '2', 'liv_posicao' => '25', 'liv_nome' => 'III João']);
        Livros::create(['liv_id' => '65', 'liv_tes_id' => '2', 'liv_posicao' => '26', 'liv_nome' => 'Judas']);
        Livros::create(['liv_id' => '66', 'liv_tes_id' => '2', 'liv_posicao' => '27', 'liv_nome' => 'Apocalipse']);
    }
}