<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `leitura`
//

// `leitura`.`testamentos`
$testamentos = array(
  array('tes_id' => '1','tes_nome' => 'Antigo Testamento','created_at' => '2016-03-02 14:12:23','updated_at' => '2016-03-02 14:12:23'),
  array('tes_id' => '2','tes_nome' => 'Novo Testamento','created_at' => '2016-03-02 14:12:23','updated_at' => '2016-03-02 14:12:23')
);
