<?php

DB::table('versoes')->create(['vrs_id' => '0','vrs_nome' => 'Não Identificada']);
DB::table('versoes')->create(['vrs_id' => '1','vrs_nome' => 'Almeida Revisada Imprensa Bíblica']);
DB::table('versoes')->create(['vrs_id' => '2','vrs_nome' => 'Almeida Corrigida e Revisada Fiel']);
DB::table('versoes')->create(['vrs_id' => '3','vrs_nome' => 'Nova Versão Internacional']);
DB::table('versoes')->create(['vrs_id' => '4','vrs_nome' => 'Sociedade Bíblica Britânica']);
DB::table('versoes')->create(['vrs_id' => '5','vrs_nome' => 'Almeida Revista e Atualizada']);
