@extends('index')

@section('title', '- Bíblia')

@section('scripts')
        <!-- Base core CSS -->
<link href="/css/blog.css" rel="stylesheet">
@stop

@section('content')
    <div id="top" class="blog-masthead">
        <div class="container">
            <div class="blog-nav">
                @include('include/nav')
            </div>
        </div>
    </div>
    <div class="container">
        <div class="blog-header">
            <h1 class="blog-title">Leitura Biblica</h1>
            <p class="lead blog-description">Escolha seu livro e tenha uma excelente leitura.</p>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 blog-main">
                <div class="blog-post">
                    <!-- Livro -->
                    @foreach ($Livro as $Livro)
                        <ul role="tablist" class="nav nav-tabs">
                            <li><a href="/biblia">Bíblia</a></li>
                            <li class="active" role="presentation"><a href="/biblia/{{ $Livro->liv_abr }}">{{ $Livro->liv_nome }}</a></li>
                        </ul>
                        @endforeach
                        <!-- Capitulos -->
                        @for ($i = 0; $i < $Count->ver_capitulo; $i++)
                            <a href="/biblia/{{ $Livro->liv_abr }}/{{ $i+1 }}"><span class="badge">{{ $i+1 }}</span></a>
                        @endfor
                </div><!-- /.leitura-post -->
            </div><!-- /.blog-main -->
        </div><!-- /.row -->
    </div><!-- /.container -->
    @include('include/rodape')
@stop