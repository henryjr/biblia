@extends('index')

@section('title', '- Bíblia')

@section('scripts')
        <!-- Base core CSS -->
<link href="/css/blog.css" rel="stylesheet">
@stop

@section('content')
    <div id="top" class="blog-masthead">
        <div class="container">
            <div class="blog-nav">
                @include('include/nav')
            </div>
        </div>
    </div>
    <div class="container">
        <div class="blog-header">
            <h1 class="blog-title">Leitura Biblica</h1>
            <p class="lead blog-description">Escolha seu livro e tenha uma excelente leitura.</p>
        </div>
        <div class="row">
            <div class="col-sm-12 blog-main">
                <div class="blog-post">
                    <div class="row space-50">
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="active" role="presentation"><a href="/biblia">Antigo Testamento</a></li>
                        </ul>
                        <ul>
                            @foreach ($antigo as $antigo)
                                @if ($antigo->liv_tes_id == 1)
                                    <li><a href="/biblia/{{ $antigo->liv_abr }}" class="btn btn-primary">{{ $antigo->liv_nome }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="row">
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="active" role="presentation"><a href="/biblia">Novo Testamento</a></li>
                        </ul>
                        <ul>
                            @foreach ($novo as $novo)
                                @if ($novo->liv_tes_id == 2)
                                    <li><a href="/biblia/{{ $novo->liv_abr }}" class="btn btn-primary">{{ $novo->liv_nome }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div><!-- /.leitura-post -->
            </div><!-- /.blog-main -->
        </div><!-- /.row -->
    </div><!-- /.container -->

    @include('include/rodape')
@stop