<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="Bíblia">
<meta name="author" content="Henrique Monteiro Junior">
<link rel="icon" href="/bootstrap/docs/favicon.ico">
<title>Bíblica @yield('title')</title>
@include('include/scripts')
@yield('scripts')
</head>
<body>

@yield('content')

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/bootstrap/docs/assets/js/vendor/jquery.min.js"></script>
<script src="/bootstrap/docs/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
@yield('scriptsFooter')
</body>
</html>
