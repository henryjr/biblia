@extends('index')

@section('title', '- Acesso')

@section('scripts')
        <!-- Base core CSS -->
<link href="/css/blog.css" rel="stylesheet">
<link href="/css/signin.css" rel="stylesheet">
@stop

@section('content')

    <div id="top" class="blog-masthead">
        <div class="container">
            <div class="blog-nav">
                @include('include/nav')
            </div>
        </div>
    </div>

    <div class="container">

        <form class="form-signin">
            <h2 class="form-signin-heading">Controle de Acesso</h2>
            <label for="inputEmail" class="sr-only">E-mail</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="E-mail" required autofocus>
            <label for="inputPassword" class="sr-only">Senha</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Senha" required>
            <button class="btn btn-lg btn-primary btn-block space-25" type="submit">Acessar</button>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <a href="/recovery" class="btn btn-lg btn-warning btn-block space-25">Recuperar</a>
                </div>
                <div class="col-md-6 col-sm-12">
                    <a href="/register" class="btn btn-lg btn-success btn-block space-25">Cadastrar-se</a>
                </div>
            </div>
        </form>

    </div> <!-- /container -->

    @include('include/rodape')
    @stop

    @section('scriptsFooter')
            <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
@stop