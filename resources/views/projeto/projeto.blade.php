@extends('index')

@section('title', '- Projeto')

@section('scripts')
        <!-- Base core CSS -->
<link href="/css/blog.css" rel="stylesheet">
@stop

@section('content')

    <div id="top" class="blog-masthead">
        <div class="container">
            <div class="blog-nav">
                @include('include/nav')
            </div>
        </div>
    </div>

    <div class="container">
        <div class="blog-header">
            <h1 class="blog-title">Sobre o Projeto</h1>
            <p class="lead blog-description">Escolha seu livro e tenha uma excelente leitura.</p>
        </div>
        <div class="row">
            <div class="col-sm-12 blog-main">
                <div class="project-post">
                    <p>Cover is a one-page template for building simple and beautiful home pages. Download, edit the text, and add your own fullscreen background photo to make it your own.</p>
                </div><!-- /.leitura-post -->
            </div><!-- /.blog-main -->
        </div><!-- /.row -->
    </div><!-- /.container -->

    @include('include/rodape')
@stop