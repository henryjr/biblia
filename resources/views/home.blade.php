@extends('index')

@section('title', '- Home')

@section('scripts')
        <!-- Base core CSS -->
<link href="/css/style.css" rel="stylesheet">
@stop

@section('content')
    <div class="site-wrapper">
        <div class="site-wrapper-inner">
            <div class="cover-container">

                <div class="masthead clearfix">
                    <div class="inner">
                        <!-- nav -->
                            @include('include/nav')
                    </div>
                </div>

                <!-- begin -->
                <div class="inner cover">
                    <h1 class="cover-heading">Leitura Biblíca</h1>
                    <p class="lead">Cover is a one-page template for building simple and beautiful home pages.
                        Download, edit the text, and add your own fullscreen background photo to make it your own.</p>
                    <p class="lead">
                        Bem vindo, <code>{{ $name or 'visitante' }}</code>.
                    </p>
                    <p class="lead">
                        <a href="/access" class="btn btn-lg btn-primary">Acesse</a>
                        <a href="/register" class="btn btn-lg btn-warning">Cadastre-se</a>
                    </p>
                </div>

                <!-- footer -->
                <div class="mastfoot">
                    <div class="inner">
                        <p>
                            <a href="http://getbootstrap.com" target="_blank">Bootstrap</a> |
                            <a href="https://twitter.com/henryjr1">@henryjr1</a> |
                            Usuários: <span class="badge">42</span>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop